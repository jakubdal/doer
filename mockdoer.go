package doer

import "net/http"

// MockHTTPDoer is used for mocking requests done with HTTPDoer
type MockHTTPDoer struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

// Do calls underlying DoFunc method do mock doing a HTTP request
func (md MockHTTPDoer) Do(req *http.Request) (*http.Response, error) {
	return md.DoFunc(req)
}
