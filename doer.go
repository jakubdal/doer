package doer

import "net/http"

// HTTPDoer is a mockable and cusotmizable entity for performing HTTP requests
type HTTPDoer interface {
	Do(req *http.Request) (*http.Response, error)
}
